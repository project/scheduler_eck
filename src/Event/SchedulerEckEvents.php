<?php

namespace Drupal\scheduler_eck\Event;

/**
 * Lists the six events dispatched by Scheduler relating to ECK entities.
 */
class SchedulerEckEvents {

  /**
   * The event triggered after an ECK entity is published immediately.
   *
   * This event allows modules to react after an ECK entity is
   * published immediately. The event listener method receives a
   * \Drupal\Core\Entity\EntityInterface instance.
   *
   * @Event
   *
   * @see \Drupal\scheduler\Event\SchedulerEvent
   *
   * @var string
   */
  const PUBLISH_IMMEDIATELY = 'scheduler.eck_publish_immediately';

  /**
   * The event triggered after an ECK entity is published via cron.
   *
   * This event allows modules to react after an ECK entity is published.
   * The event listener method receives a \Drupal\Core\Entity\EntityInterface
   * instance.
   *
   * @Event
   *
   * @see \Drupal\scheduler\Event\SchedulerEvent
   *
   * @var string
   */
  const PUBLISH = 'scheduler.eck_publish';

  /**
   * The event triggered before an ECK entity is published immediately.
   *
   * This event allows modules to react before an ECK entity is published
   * immediately. The event listener method receives a
   * \Drupal\Core\Entity\EntityInterface instance.
   *
   * @Event
   *
   * @see \Drupal\scheduler\Event\SchedulerEvent
   *
   * @var string
   */
  const PRE_PUBLISH_IMMEDIATELY = 'scheduler.eck_pre_publish_immediately';

  /**
   * The event triggered before an ECK entity is published via cron.
   *
   * This event allows modules to react before an ECK entity is published.
   * The event listener method receives a \Drupal\Core\Entity\EntityInterface
   * instance.
   *
   * @Event
   *
   * @see \Drupal\scheduler\Event\SchedulerEvent
   *
   * @var string
   */
  const PRE_PUBLISH = 'scheduler.eck_pre_publish';

  /**
   * The event triggered before an ECK entity is unpublished via cron.
   *
   * This event allows modules to react before an ECK entity is unpublished. The
   * event listener method receives a \Drupal\Core\Entity\EntityInterface
   * instance.
   *
   * @Event
   *
   * @see \Drupal\scheduler\Event\SchedulerEvent
   *
   * @var string
   */
  const PRE_UNPUBLISH = 'scheduler.eck_pre_unpublish';

  /**
   * The event triggered after an ECK entity is unpublished via cron.
   *
   * This event allows modules to react after an ECK entity is unpublished.
   * The event listener method receives a \Drupal\Core\Entity\EntityInterface
   * instance.
   *
   * @Event
   *
   * @see \Drupal\scheduler\Event\SchedulerEvent
   *
   * @var string
   */
  const UNPUBLISH = 'scheduler.eck_unpublish';

}
