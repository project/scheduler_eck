<?php

namespace Drupal\scheduler_eck\Plugin\Scheduler;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\scheduler\SchedulerPluginBase;
use Drupal\scheduler_eck\Event\SchedulerEckEvents;

/**
 * Plugin for ECK entity types.
 *
 * @package Drupal\Scheduler\Plugin\Scheduler
 *
 * @SchedulerPlugin(
 *  id = "scheduler_eck",
 *  dependency = "eck",
 *  deriver = "Drupal\scheduler_eck\Plugin\Derivative\SchedulerEckDeriver",
 * )
 */
class EckScheduler extends SchedulerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function schedulerEventClass() {
    return SchedulerEckEvents::class;
  }

}
