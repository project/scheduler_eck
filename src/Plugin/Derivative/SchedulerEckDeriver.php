<?php

namespace Drupal\scheduler_eck\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates scheduler plugins for all ECK entity types.
 */
class SchedulerEckDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if ($this->derivatives) {
      return $this->derivatives;
    }

    $entityTypes = $this->entityTypeManager
      ->getStorage('eck_entity_type')
      ->loadMultiple();

    foreach ($entityTypes as $entityType) {
      $derivativeKey = sprintf('%s:%s', $base_plugin_definition['id'], $entityType->id());
      $definition = $this->entityTypeManager->getDefinition($entityType->id());
      $statusFieldName = $definition->getKey('published');

      if (!$entityType->get($statusFieldName)) {
        continue;
      }

      $description = $this->t('Provides support for scheduling @entityType entities', [
        '@entityType' => $entityType->label(),
      ]);

      $this->derivatives[$entityType->id()] = $base_plugin_definition;
      $this->derivatives[$entityType->id()]['id'] = $derivativeKey;
      $this->derivatives[$entityType->id()]['label'] = $entityType->label();
      $this->derivatives[$entityType->id()]['description'] = $description;
      $this->derivatives[$entityType->id()]['entityType'] = $entityType->id();
    }

    return $this->derivatives;
  }

}
